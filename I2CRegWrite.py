#!/usr/bin/python3
# -*- coding: utf-8 -*- 

import sys
import csv
from zerowired.I2C import I2C

def usage():
	"""^(^_^)^"""
	print(sys.argv[0] + ' [I2C address(HEX)] [config file]')
	print(sys.argv[0] + ' 0x60 ./dir/si5351_2.8224mhz.txt')

def app_main():
	"""application main procedure"""
	if len(sys.argv) < 3:
		usage()
		return;
	with open(sys.argv[2], 'r', encoding='UTF-8', errors='', newline='' ) as csv_file:
		dev = I2C(int(sys.argv[1], 0))
		conf = csv.reader(csv_file, delimiter=",", doublequote=False, lineterminator="\r\n", quotechar='"', skipinitialspace=True)
		fields = next(conf)
		for line in conf:
			print('{0}=0x{1:04X}:{2}=0x{3:02X}'.format(fields[0], int(line[0], 0), fields[1], int(line[1], 0)))
			adr = int(line[0], 0)
			val = int(line[1], 0)
			dev.write_byte(adr, val)

if __name__ == '__main__':
	app_main()