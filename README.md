# Si5351A configuration sample

## USAGE
    python3 I2CRegWrite.py [I2C address(HEX)] [config file]

    example)
    python3 I2CRegWrite.py 0x60 ./config/si5351_2.8224mhz.txt
