#!/usr/bin/python3
# -*- coding: utf-8 -*- 

from smbus2 import SMBus
from time import sleep


class I2C:
    """I2C controll class"""

    def __init__(self, device_address):
        """initialize"""
        self._bus = SMBus(1)
        self._device_address = device_address
        print('device address=0x{0:02X}'.format(self._device_address))

    def __del__(self):
        """clean up"""
        self._bus.close()

    def read_byte(self, offset):
        """1 byte read"""
        data = self._bus.read_byte_data(
            self._device_address, offset)
        return data

    def write_byte(self, offset, value):
        """1 byte write"""
        self._bus.write_byte_data(self._device_address, offset, value)

    def read_word(self, offset):
        """2 bytes read"""
        data = self._bus.read_word_data(
            self._device_address, offset)
        return data

    def write_word(self, offset, value):
        """2 bytes write"""
        self._bus.write_word_data(self._device_address, offset, value)

    def read_dword(self, offset):
        """4 bytes read"""
        data = self._bus.read_i2c_block_data(
            self._device_address, offset)
        return data

    def write_dword(self, offset, value):
        """4 bytes write"""
        self._bus.write_i2c_block_data(self._device_address, offset, value)

    def read_block(self, offset, length):
        """n bytes read"""
        data = self._bus.read_i2c_block_data(
            self._device_address, offset, length)
        return data

    def write_block(self, offset, data):
        """n bytes write"""
        self._bus.write_i2c_block_data(self._device_address, offset, data)
        return True
